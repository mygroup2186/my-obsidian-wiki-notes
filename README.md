# my-obsidian-wiki-notes

This project demonstrates how to set up your Obsidian notes to be published using GitLab Pages and MkDocs.

See https://mygroup2186.gitlab.io/my-obsidian-wiki-notes for the rendered site.
